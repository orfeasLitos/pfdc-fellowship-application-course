% NSF proposal generation template style file.
% based on latex stylefiles written by Stefan Llewellyn Smith and
% Sarah Gille, with contributions from other collaborators.
%
\documentclass[12pt]{nsfproposal}

\usepackage{amsmath}
\usepackage{times}
\usepackage{graphviz}
\usepackage{import}
\usepackage{hyperref}

\bibliographystyle{splncs04}

\begin{document}

\begin{center}
{\Large\textbf{Research Proposal}}

{\large\textbf{The Future is Multi-Agent: Applications and Analysis}}
\begin{align*}
\text{Applicant:} & \text{ Dr.\ Orfeas Stefanos Thyfronitis Litos}
\end{align*}
\ \\
{\small Department of Computing}
\ \\
{\small Imperial College London}

\end{center}

\section{Background}
Practical security consists of the indispensable task of constraining the
functionalities of computational systems to the minimal required. As Gene
Spafford said, ``the only truly secure system is one that is powered off''.
\emph{Cryptography}, which is one of my pillars of expertise, underpins every
secure computer protocol. There is a range of applications that can only be
performed if the underlying designs are meticulously secured. For example,
e-commerce requires end-to-end authentication and encryption over the web
(a.k.a.\ Transport Layer Security -- TLS).

In the context of complex multi-agent applications, determination of the exact
security requirements necessitates the analysis of the implicated parties'
incentives. In the case of online payments over TLS, the payer needs encryption
to hide its card details from prying eyes, whilst authentication of the payer is
demanded by the payee to ensure that the money is received from the correct
party. Incentive analysis lies within the remit of \emph{Game Theory}, my second
pillar of specialisation.

My third pillar of expertise, i.e., \emph{Distributed Ledger Technologies}
(DLTs), which are commonly embodied by blockchains, constitute the
connecting tissue needed to build and analyse multi-agent systems in a manner
that guarantees security and incentive-compatibility. Built on a strong
cryptographic and game-theoretic basis, their \emph{immutability} ensures that
the log of agent transactions are reliably \emph{available}. It further enables
designs in which \emph{accountability} for participant activity is guaranteed.
The open nature of DLTs serves as an excellent backbone for \emph{open standards
specification}, and their inherent resilience against malicious parties provide
the commonly required \emph{decentralisation} capabilities.

\section{Aims and Research Questions}
I aspire to apply the aforementioned principles in expanding the scope of
feasibility of security-critical \emph{multi-agent protocols}, as well as
improve and streamline them, while minimising the \emph{trust} needed between
parties within both existing and novel deployments. Among others, I intend to
design \emph{cyberphysical systems}. This setting is becoming increasingly
relevant over the last years. A rising number of modern applications rely on
interconnected physical devices such as Unmanned Aerial Vehicles (UAVs) that
perform complex tasks in an automated manner. Our society increasingly relies on
such infrastructure for access to, e.g., emergency response services, ubiquitous
internet connectivity, cargo and passenger transportation, as well as weather
prediction. Such schemes need robust and trustworthy data storage, reporting,
and accountability capabilities. Moreover, relying on a single provider for all
devices and software is becoming progressively untenable given the dangers of
\emph{single points of failure} in the context of critical, always-on
infrastructure. Therefore the need for open standards and implementations
resilient to malicious counterparties arises. My previous participation in the
BLOCC project, which was funded by Innovate
UK\footnote{\url{https://www.ukri.org/publications/innovate-uk-funded-projects-since-2004/}}
and concerns secure logging of sensor data in the context of the shipping
industry, lies on the interface between the physical and the digital worlds and
has provided me with the experience needed for tackling the aforementioned
challenges.

Multi-agent systems can be naturally extended to include human participants. An
area of discourse that becomes increasingly relevant is that of \emph{digital
citizenship}. As our interactions take place more and more within virtual
spaces, individuals need the ability to transfer their identities, histories,
and credentials across a diverse and ever-changing variety of social media
platforms. Furthermore, being able to verify \emph{content provenance} without
trusting the platform in which it was published gains importance, especially
given the rise of malevolent fake news facilitated by ``deepfake'' technology.
DLTs can underpin the design of \emph{public digital spaces} and foment a new
age for \emph{democratic participation}.

\section{Methodology}
The aforementioned objectives will be achieved using a combination of
methodologies. First and foremost, the system goals and mechanisms will be
defined using the language of cryptography. The primary tool for this, which is
especially effective for multi-party protocols, will be simulation-based
analysis~\cite{DBLP:books/sp/17/Lindell17}, possibly expanded to Universally
Composable Security~\cite{DBLP:conf/focs/Canetti01} for systems that can be used
as a component of larger systems and thus require formal composability
guarantees --- for example, a fully automated resilient logging infrastructure.
My previous extensive use of these frameworks~\cite{DBLP:conf/csfw/KiayiasL20}
have provided the required competence for using them effectively.

In parallel, many systems require sufficient motivation for its agents to stay
responsive and act in the prescribed manner. In such cases, an incentive
analysis is additionally needed. This methodology entails formally defining the
utility functions of the various agents and proving that them following the
prescribed protocol constitutes a desireable \emph{Nash equilibrium}, which
guarantees that agents cannot benefit by deviating from it. I have used this
approach in previous
work~\cite{DBLP:conf/fc/AvarikiotiLW20,DBLP:conf/fc/AvarikiotiL22} and can
therefore readily leverage it when needed.

The toolbox for building the desired protocols contains, among others,
established cryptographic techniques such as multi-party
computation~\cite{DBLP:conf/focs/Yao82b}, threshold
signatures~\cite{shoup00practical}, and Distributed Ledger
Technology~\cite{gkl}. I have made extensive use of
DLTs~\cite{DBLP:conf/fc/AvarikiotiL22,DBLP:conf/fc/AvarikiotiLW20,DBLP:conf/fc/LitosZ17,DBLP:conf/csfw/KiayiasL20}
in previous work.

\section{Outcomes \& Impact}

The intended outcome of this line of work is an expansion of the breadth and
depth of knowledge on the topic of multi-agent systems, as well as a number of
practical, concrete, novel systems that can promote societal growth and
prosperity. The outputs will achive recognition as a series of academic
publications in highly esteemed venues that specialise in Systems, Cryptography,
Game Theory, and DLTs. Via this work, I furthermore aspire to contribute to the
prestige of this eminent institution as well as broaden the reach of my
capabilities via new collaborations and further ability to procure resources to
invest in future research.

\bibliography{src/references}
\end{document}
