More and more aspects of our daily life are handled by machines. What is more, the tasks they can perform become more complex every year and often require them to interact with other machines. These interactions between agents pose a significant security risk. My research has to do with designing these agents, as well as the exact way they should interact with others, with a security-first mindset. I am an expert in Cryptography, Game Theory and blockchains (such as Bitcoin), so my background is very suited to this topic.

More specific project
More details on risks and methods
